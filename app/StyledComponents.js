import styled from 'styled-components';
import { darken, math } from 'polished';
import { Link } from 'react-router-dom';

export const FormGroup = styled.div``;
export const Label = styled.label`
  padding: 0 10px;
  position: relative;
`;
export const Input = styled.input`
  width: 100%;
  margin: 5px 0;
  padding: 10px;
  border-bottom: 2px solid ${props => props.theme.primaryBgColor};
  &:focus {
    outline: none;
  }
  &:focus + label {
    font-style: italic;
    font-size: 80%;
  }
`;
export const Select = styled.select`
  width: 100%;
  margin: 5px 0;
  padding: 10px;
  border-bottom: 2px solid ${props => props.theme.primaryBgColor};
  &:focus {
    outline: none;
  }
  &:focus + label {
    font-style: italic;
    font-size: 80%;
  }
`;
export const TextArea = styled.textarea`
  width: 100%;
  margin: 5px 0;
  padding: 10px;
  border-bottom: 2px solid ${props => props.theme.primaryBgColor};
  &:focus {
    outline: none;
  }
  &:focus + label {
    font-style: italic;
    font-size: 80%;
  }
`;
export const FormText = styled.div``;
export const Button = styled.button`
  width: ${props => props.width || '100%'};
  text-align: center;
  padding: 2px 5px;
  background-color: ${props => props.theme.primaryBgColor};
  cursor: pointer;
  &:visited,
  &:active,
  &:focus {
    outline: none;
  }
  &:hover {
    background-color: ${props => darken(0.15, props.theme.primaryBgColor)};
  }
`;

export const NavbarBrand = styled(Link)`
  height: ${props => props.theme.headerHeight};
  line-height: ${props => props.theme.headerHeight};
  vertical-align: middle;
  font-size: ${props => math(`${props.theme.headerHeight}* 0.4`)};
  &:active {
    color: inherit;
  }
`;
export const BrandImage = styled.img`
  width: ${props => math(`${props.theme.headerHeight} * 2`)};
  height: ${props => math(`${props.theme.headerHeight} * 1.5`)};
`;
export const NavLink = styled(Link)`
  height: ${props => props.theme.headerHeight};
  line-height: ${props => props.theme.headerHeight};
  vertical-align: middle;
  padding: 0 10px;
`;
export const NavItemNormal = styled.h4`
  display: inline-block;
  margin: 0;
  padding: 0;
  vertical-align: middle;
`;

export const Navbar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 5px;
  height: ${props => props.theme.headerHeight};
  margin-bottom: 15px;
`;
export const Nav = styled.ul`
  height: ${props => props.theme.headerHeight};
  line-height: ${props => props.theme.headerHeight};
  vertical-align: middle;
  list-style: none;
  margin: 0;
  padding: 0;
`;
export const NavItem = styled.li`
  display: inline-block;
  height: ${props => props.theme.headerHeight};
  padding: 0;
`;
