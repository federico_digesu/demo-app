/**
 *
 * Header
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavItemNormal,
  NavLink,
} from 'StyledComponents';

function Header({ user, signout }) {
  return (
    <Navbar>
      <NavbarBrand to="/">Slot Machine</NavbarBrand>
      <Nav className="ml-auto" navbar>
        {user.id ? (
          <React.Fragment>
            <NavItem>
              <NavLink to="/countries">Countries</NavLink>
            </NavItem>
            <NavItemNormal>Hello {user.name}</NavItemNormal>
            <NavItem>
              <NavLink to="/" onClick={signout}>
                Logout
              </NavLink>
            </NavItem>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <NavItem>
              <NavLink to="/login">Login</NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/signup">Signup</NavLink>
            </NavItem>
          </React.Fragment>
        )}
      </Nav>
    </Navbar>
  );
}

Header.propTypes = {
  user: PropTypes.shape({}).isRequired,
  signout: PropTypes.func.isRequired,
};

export default Header;
