/**
 *
 * LoginForm
 *
 */

import React from 'react';
import { Field, reduxForm } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import InputControl from 'components/InputControl';

function LoginForm({ handleSubmit, submitFunc, error, submitting }) {
  return (
    <React.Fragment>
      {error && <p className="text-danger">{error}</p>}
      <form onSubmit={handleSubmit(submitFunc)} autoComplete="off">
        <Field
          name="email"
          label="Email"
          placeholder="Email"
          component={InputControl}
          type="email"
        />
        <Field
          name="password"
          label="Password"
          placeholder="Password"
          component={InputControl}
          type="password"
        />
        {error && <div>{error}</div>}
        <button type="submit" disabled={submitting === true}>
          LOGIN
        </button>
      </form>
    </React.Fragment>
  );
}

LoginForm.propTypes = {
  submitFunc: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.any,
  submitting: PropTypes.bool.isRequired,
};
export default reduxForm({
  // a unique name for the form
  form: 'loginForm',
})(LoginForm);
