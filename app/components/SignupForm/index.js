/**
 *
 * SignupForm
 *
 */

import React from 'react';
import { Field, reduxForm } from 'redux-form/immutable';
import InputControl from 'components/InputControl';
import PropTypes from 'prop-types';

function SignupForm({ submitFunc, handleSubmit, submitting }) {
  return (
    <form onSubmit={handleSubmit(submitFunc)} autoComplete="off">
      <Field
        name="name"
        label="Full Name"
        placeholder="Enter your full name"
        component={InputControl}
        type="text"
      />
      <Field
        name="email"
        label="Email"
        placeholder="Email"
        component={InputControl}
        type="email"
      />
      <Field
        name="password"
        label="Choose Password"
        placeholder="Password"
        component={InputControl}
        type="password"
      />
      <Field
        name="repeat_password"
        label="Repeat Password"
        placeholder="Repeat Password"
        component={InputControl}
        type="password"
      />
      <button type="submit" disabled={submitting === true}>
        Submit
      </button>
    </form>
  );
}

SignupForm.propTypes = {
  submitFunc: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  // a unique name for the form
  form: 'signupForm',
})(SignupForm);
