/**
 *
 * InputControl
 *
 */
import React from 'react';
import PropTypes from 'prop-types';

function InputControl({
  input,
  label,
  placeholder,
  type,
  meta: { touched, error },
}) {
  return (
    <div className="form-group">
      <label>{label}</label>
      <div>{touched && error && <div className="danger">{error}</div>}</div>
      <input {...input} placeholder={placeholder} type={type} />
    </div>
  );
}

InputControl.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  meta: PropTypes.object,
};

export default InputControl;
