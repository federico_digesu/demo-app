export function getLocalStorageToken() {
  return localStorage.getItem('token');
}

export function setLocalStorageToken(token) {
  localStorage.setItem('token', token);
}

export function removeLocalStorageToken() {
  localStorage.removeItem('token');
}

export function setUser(user) {
  localStorage.setItem('user', JSON.stringify(user));
}

export function getUser() {
  const user = localStorage.getItem('user');
  return JSON.parse(user);
}

export function removeUser() {
  localStorage.removeItem('user');
}
