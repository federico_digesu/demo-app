import axios from 'axios';
import { getLocalStorageToken } from './localStorage';

axios.defaults.baseURL = '/api';

axios.interceptors.request.use(
  config => {
    // Do something before request is sent
    const token = getLocalStorageToken();
    if (token) {
      config.headers.Authorization = `JWT ${token}`; //eslint-disable-line
    }
    return config;
  },
  error => Promise.reject(error),
);

export default axios;
