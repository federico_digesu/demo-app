import { createGlobalStyle } from 'styled-components';
import { darken } from 'polished';

const GlobalStyle = createGlobalStyle`
/* html, */
  body {
    margin: 0 auto;
    background-color: #fff;
  }
  
  html {
    font-family: 'Ropa Sans', sans-serif;
    font-size: 16px;
    color: #131313;
  }
  
  label {
    font-size: 1.1em;
    margin-bottom: 0.2em;
  }
  
    /* form input fields */
  select {
    display: block;
    background: none;
    cursor: pointer;
    width: 100%;
  }
  
  input,
  textarea, select {
    display:block;
    width: 100%;
    padding: 0.5em;
    font-size: 1.1em;
    line-height: 1.2;
    text-align: left;
    color: #131313;
    border-bottom:2px solid #CC9933;
  }
  .creatable-control > div{
    border: none;
    border-bottom:2px solid #CC9933;
  }
  
  input,
  textarea, select:focus {
    outline: 0 !important;
  }
  
  textarea {
    resize: none;
    margin-top: 0.5em;
    height: 3em;
  }
    form .danger{
    color: red;
    text-align: left;
  }
    button:disabled {
    background: #ddd; 
  }
  
  button:focus,
  input[type=submit]:focus,
  .cta:focus,.cta-light:focus {
    outline: 0 !important;
  }
  button{
    background: #00ba4d;
    padding: 5px;
    margin: 5px;
    width: 100%;
    color: white;
    cursor: pointer;
  }
 
  .login_page, .signup_page{
    background-color: ${darken(0.1, '#fff')};
    width: 60%;
    margin: 0 auto;
    padding: 15px;
  }
  
  .countries_container{
    display: flex;
    justify-content: space-around;
    padding: 10px;
  }
  
  .country_lists{
    margin: 0 20px;
  }
  .home_page{
    background-color: ${darken(0.1, '#fff')};
    width: 60%;
    margin: 0 auto;
    padding: 15px;
    .content{
      display: flex;
      flex-direction: column;
      align-items: center;
      .wheels{
        display: flex;
        > div{
          border: 1px solid black;
          padding: 5px;
          margin: 5px;
          width: 50px;
          height: 35px;
        }
      }
    }
  }
`;

export default GlobalStyle;
