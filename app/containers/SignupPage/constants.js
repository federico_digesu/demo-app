/*
 *
 * SignupPage constants
 *
 */

export const DEFAULT_ACTION = 'app/SignupPage/DEFAULT_ACTION';
export const SIGNUP_PAGE_SUBMIT = 'app/SignupPage/SIGNUP_PAGE_SUBMIT';
