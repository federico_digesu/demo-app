/**
 *
 * SignupPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import SignupForm from 'components/SignupForm';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectSignupPage from './selectors';
import reducer from './reducer';
import saga from './saga';

import { submit } from './actions';

/* eslint-disable react/prefer-stateless-function */
export class SignupPage extends React.Component {
  signupSubmit = payload => {
    const { dispatch } = this.props;
    dispatch(submit(payload));
  };

  render() {
    return (
      <div className="signup_page">
        <Helmet>
          <title>Slot Machine: SignupPage</title>
          <meta name="description" content="Description of SignupPage" />
        </Helmet>
        <SignupForm submitFunc={this.signupSubmit} />
      </div>
    );
  }
}

SignupPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  signupPage: makeSelectSignupPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'signupPage', reducer });
const withSaga = injectSaga({ key: 'signupPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(SignupPage);
