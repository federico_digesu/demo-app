import { takeLatest, call, put, select, all } from 'redux-saga/effects';
import { stopSubmit } from 'redux-form';
import { push } from 'connected-react-router';
import axios from 'utils/axios';

import { SIGNUP_PAGE_SUBMIT } from './constants';

// Individual exports for testing

function* handleSubmit(action) {
  const payload = action.payload.toJS();
  try {
    const res = yield axios.post('/auth/register', payload);
    if (res.status === 201) {
      yield put(push('/login'));
    }
  } catch (error) {
    if (error.response.status === 409) {
      yield put(stopSubmit('signupForm', error.response.data));
    }
  }
}

function* watchSubmit() {
  yield takeLatest(SIGNUP_PAGE_SUBMIT, handleSubmit);
}

export default function* signupPageSaga() {
  yield all([watchSubmit()]);
}
