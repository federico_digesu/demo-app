/*
 *
 * SignupPage actions
 *
 */

import { DEFAULT_ACTION } from './constants';
import { SIGNUP_PAGE_SUBMIT } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function submit(payload) {
  return {
    type: SIGNUP_PAGE_SUBMIT,
    payload,
  };
}
