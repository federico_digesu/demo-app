import { takeLatest, put, all } from 'redux-saga/effects';
import axios from 'utils/axios';

import { setUser, appInitialize } from 'containers/App/actions';
import { APP_GET_USER } from './constants';

function* handleGetAuth(action) {
  const token = action.payload;
  try {
    const res = yield axios.get(`/auth/getUserInfo/${token}`);
    if (res.status === 200) {
      yield put(setUser(res.data));
      yield put(appInitialize());
    }
  } catch (error) {
    if (error.response.status === 409) {
      yield put(appInitialize());
    }
  }
}

function* watchGetUser() {
  yield takeLatest(APP_GET_USER, handleGetAuth);
}

// Individual exports for testing
export default function* loginPageSaga() {
  yield all([watchGetUser()]);
}
