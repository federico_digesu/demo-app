/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, Redirect } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
// import { ToastContainer } from 'react-toastify';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { compose } from 'redux';

import HomePage from 'containers/HomePage/Loadable';
import CountriesPage from 'containers/CountriesPage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import SignupPage from 'containers/SignupPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

import 'react-toastify/dist/ReactToastify.css';
import Header from 'components/Header';

import { ConnectedRouter } from 'connected-react-router/immutable';
import history from 'utils/history';
import {
  getLocalStorageToken,
  removeLocalStorageToken,
} from 'utils/localStorage';
import GlobalStyle from '../../global-styles';
import { AppLoading, AppSpinner, AppLoadingWrapper } from './StyledComponents';
import makeSelectApp from './selectors';
import reducer from './reducer';
import saga from './saga';

import { getUser, signoutUser, appInitialize } from './actions';

/* eslint-disable react/prefer-stateless-function */
export class App extends React.Component {
  signout = evt => {
    evt.preventDefault();
    const { dispatch } = this.props;
    removeLocalStorageToken();
    dispatch(signoutUser());
  };

  componentDidMount() {
    const token = getLocalStorageToken();
    const { dispatch } = this.props;
    if (token) {
      dispatch(getUser(token));
    } else {
      dispatch(appInitialize());
    }
  }

  render() {
    const { app } = this.props;
    if (app.initialized === false) {
      return (
        <AppLoading>
          <AppLoadingWrapper>
            <AppSpinner />
          </AppLoadingWrapper>
        </AppLoading>
      );
    }
    return (
      <ConnectedRouter history={history}>
        <React.Fragment>
          <Header user={app.user} signout={this.signout} />
          <div className="container">
            <div className="main">
              <Switch>
                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/signup" component={SignupPage} />
                <PrivateRoute
                  isAuthorized={!!app.user.id}
                  path="/countries"
                  component={CountriesPage}
                  exact
                />
                <PrivateRoute
                  isAuthorized={!!app.user.id}
                  path="/"
                  component={HomePage}
                />
                <Route component={NotFoundPage} />
              </Switch>
            </div>
          </div>
          {/* temporary disabling toastr */}
          {/* <ToastContainer autoClose={3000} /> */}
          <GlobalStyle />
        </React.Fragment>
      </ConnectedRouter>
    );
  }
}

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props => (rest.isAuthorized === true) ? // eslint-disable-line
        (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  app: PropTypes.shape({}).isRequired,
};

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'app', reducer });
const withSaga = injectSaga({ key: 'app', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(App);
