/*
 *
 * HomePage constants
 *
 */

export const APP_SET_USER = 'app/APP_SET_USER';
export const APP_GET_USER = 'app/APP_GET_USER';
export const APP_USER_SIGN_OUT = 'app/APP_USER_SIGN_OUT';
export const APP_INITIALIZE = 'app/APP_INITIALIZE';
