/*
 *
 * HomePage actions
 *
 */

import { APP_GET_USER, APP_INITIALIZE, APP_SET_USER, APP_USER_SIGN_OUT } from './constants';

export function appInitialize() {
  return {
    type: APP_INITIALIZE,
  };
}

export function getUser(token) {
  return {
    type: APP_GET_USER,
    payload: token,
  };
}

export function setUser(payload) {
  return {
    type: APP_SET_USER,
    payload,
  };
}

export function signoutUser() {
  return {
    type: APP_USER_SIGN_OUT,
  };
}
