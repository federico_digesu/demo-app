/*
 *
 * HomePage reducer
 *
 */

import { fromJS } from 'immutable';
import { APP_SET_USER, APP_USER_SIGN_OUT, APP_INITIALIZE } from './constants';

export const initialState = fromJS({
  user: {},
  initialized: false,
});

function app(state = initialState, action) {
  switch (action.type) {
    case APP_SET_USER:
      return state.set('user', fromJS(action.payload));
    case APP_USER_SIGN_OUT:
      return state.set('user', fromJS({}));
    case APP_INITIALIZE:
      return state.set('initialized', true);
    default:
      return state;
  }
}

export default app;
