import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the homePage state domain
 */

const selectApp = state => state.get('app', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by HomePage
 */

const makeSelectApp = () =>
  createSelector(selectApp, substate => substate.toJS());

export default makeSelectApp;
export { selectApp };
