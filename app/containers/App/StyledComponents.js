import styled from 'styled-components';

export const AppLoading = styled.div`
  background-color: #f07260;
  height: 100vh;
  font-size: 64px;
`;

export const AppLoadingWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const AppSpinner = styled.div`
  position: relative;
  &:before,
  &:after {
    content: '';
    position: relative;
    display: block;
  }
  &:before {
    animation: spinner 2.5s cubic-bezier(0.75, 0, 0.5, 1) infinite normal;
    width: 1em;
    height: 1em;
    background-color: #fff;
  }
  &:after {
    :after {
      animation: shadow 2.5s cubic-bezier(0.75, 0, 0.5, 1) infinite normal;
      bottom: -0.5em;
      height: 0.25em;
      border-radius: 50%;
      background-color: rgba(#000, 0.2);
    }
  }
  @keyframes spinner {
    50% {
      border-radius: 50%;
      transform: scale(0.5) rotate(360deg);
    }
    100% {
      transform: scale(1) rotate(720deg);
    }
  }
  @keyframes shadow {
    50% {
      transform: scale(0.5);
      background-color: rgba(#000, 0.1);
    }
  }
`;
