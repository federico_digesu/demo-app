import { takeLatest, put, all } from 'redux-saga/effects';
import axios from 'utils/axios';
import { toast } from 'react-toastify';
import { CALCULATE_POINTS, GET_WHEELS } from './constants';
import { getWheelsSuccess, calculatePointsSuccess } from './actions';

function* getWheels() {
  try {
    const res = yield axios.get('slotGames');
    yield put(getWheelsSuccess(res.data));
  } catch (error) {
    yield toast.error(error.response.data);
  }
}
function* calculatePoints(action) {
  try {
    const res = yield axios.post('slotGames/calculatePoints', action.payload);
    yield put(calculatePointsSuccess(res.data));
  } catch (error) {
    yield toast.error(error.response.data);
  }
}

function* watchGetWheels() {
  yield takeLatest(GET_WHEELS, getWheels);
}

function* watchCalculatePoints() {
  yield takeLatest(CALCULATE_POINTS, calculatePoints);
}

// Individual exports for testing
export default function* countriesPageSaga() {
  // See example in containers/HomePage/saga.js
  yield all([watchGetWheels(), watchCalculatePoints()]);
}
