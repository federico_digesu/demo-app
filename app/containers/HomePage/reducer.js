/*
 *
 * HomePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  ADJUST_COINS,
  CALCULATE_POINTS_SUCCESS,
  DEFAULT_ACTION,
  GET_WHEELS_SUCCESS,
} from './constants';

export const initialState = fromJS({
  coins: 20,
  wheels: {},
  resultStringArray: [],
});

function homePageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GET_WHEELS_SUCCESS:
      return state.set('wheels', fromJS(action.payload));
    case ADJUST_COINS:
      return state.set('coins', action.payload);
    case CALCULATE_POINTS_SUCCESS:
      return state
        .set('resultStringArray', action.payload.resultString)
        .set('coins', action.payload.point + state.get('coins'));
    default:
      return state;
  }
}

export default homePageReducer;
