/*
 *
 * HomePage constants
 *
 */

export const DEFAULT_ACTION = 'app/HomePage/DEFAULT_ACTION';
export const GET_WHEELS = 'app/HomePage/GET_WHEELS';
export const GET_WHEELS_SUCCESS = 'app/HomePage/GET_WHEELS_SUCCESS';
export const ADJUST_COINS = 'app/HomePage/ADJUST_COINS';
export const CALCULATE_POINTS = 'app/HomePage/CALCULATE_POINTS';
export const CALCULATE_POINTS_SUCCESS = 'app/HomePage/CALCULATE_POINTS_SUCCESS';
