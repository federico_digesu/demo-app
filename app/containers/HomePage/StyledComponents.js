import styled from 'styled-components';
// import { darken } from 'polished';

export const BannerContainer = styled.div`
  display: flex;
`;

export const BannerLeft = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 50%;
`;
export const BannerRight = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 50%;
`;

export const IntroContainer = styled.div`
  padding: 50px 25%;
  background-color: ${props => props.theme.primaryBgColor};
  color: white;
`;
