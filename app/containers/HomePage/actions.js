/*
 *
 * HomePage actions
 *
 */

import {
  ADJUST_COINS,
  CALCULATE_POINTS,
  CALCULATE_POINTS_SUCCESS,
  DEFAULT_ACTION,
  GET_WHEELS,
  GET_WHEELS_SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getWheels() {
  return {
    type: GET_WHEELS,
  };
}
export function getWheelsSuccess(payload) {
  return {
    type: GET_WHEELS_SUCCESS,
    payload,
  };
}

export function adjustCoins(payload) {
  return {
    type: ADJUST_COINS,
    payload,
  };
}

export function calculatePoints(payload) {
  return {
    type: CALCULATE_POINTS,
    payload,
  };
}

export function calculatePointsSuccess(payload) {
  return {
    type: CALCULATE_POINTS_SUCCESS,
    payload,
  };
}
