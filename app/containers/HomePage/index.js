/**
 *
 * HomePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectHomePage from './selectors';
import reducer from './reducer';
import { getWheels, adjustCoins, calculatePoints } from './actions';
import saga from './saga';

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(getWheels());
  }

  spinWheel = () => {
    const { dispatch } = this.props;
    const { coins } = this.props.homePage;
    dispatch(adjustCoins(coins - 1));
    // generating results
    dispatch(
      calculatePoints([
        Math.floor(Math.random() * 8),
        Math.floor(Math.random() * 8),
        Math.floor(Math.random() * 8),
      ]),
    );
  };

  render() {
    const { coins, resultStringArray } = this.props.homePage;
    return (
      <div className="home_page">
        <Helmet>
          <title>HomePage</title>
          <meta name="description" content="Description of HomePage" />
        </Helmet>
        <div className="content">
          <h2>Your Coins: {coins}</h2>
          <h3>Wheels</h3>
          <div className="wheels">
            <div>{resultStringArray && resultStringArray[0]}</div>
            <div>{resultStringArray && resultStringArray[1]}</div>
            <div>{resultStringArray && resultStringArray[2]}</div>
          </div>
          <button disabled={coins < 1} type="button" onClick={this.spinWheel}>
            Spin
          </button>
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  homePage: PropTypes.shape({
    coins: PropTypes.number,
    resultStringArray: PropTypes.array,
  }).isRequired,
};

const mapStateToProps = createStructuredSelector({
  homePage: makeSelectHomePage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'homePage', reducer });
const withSaga = injectSaga({ key: 'homePageSaga', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
