/*
 *
 * CountriesPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  ADD_TO_FILTERED_LIST,
  DEFAULT_ACTION,
  GET_ALL_COUNTRIES_SUCCESS,
  GET_COUNTRY_BY_ARRAY_MATCHED_SUCCESS,
  GET_COUNTRY_BY_FULL_NAME_SUCCESS,
} from './constants';

export const initialState = fromJS({
  countries: [],
  filtered: [],
  country: {},
  strings: [],
  countriesStringArrayMatched: [],
});

function countriesPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GET_ALL_COUNTRIES_SUCCESS:
      return state.set('countries', fromJS(action.payload));
    case ADD_TO_FILTERED_LIST:
      if (action.payload && action.payload.trim().length > 0) {
        const countries = state.get('countries').toJS();
        const filtered = countries.filter(
          country =>
            country.toLowerCase().indexOf(action.payload.toLowerCase()) > -1,
        );
        return state.set('filtered', fromJS(filtered));
      }
      return state.set('filtered', fromJS([]));
    case GET_COUNTRY_BY_FULL_NAME_SUCCESS:
      return state.set('country', fromJS(action.payload));
    case GET_COUNTRY_BY_ARRAY_MATCHED_SUCCESS:
      return state
        .set('strings', fromJS(action.payload.strings))
        .set('countriesStringArrayMatched', fromJS(action.payload.countries));
    default:
      return state;
  }
}

export default countriesPageReducer;
