import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the countriesPage state domain
 */

const selectCountriesPageDomain = state =>
  state.get('countriesPage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by CountriesPage
 */

const makeSelectCountriesPage = () =>
  createSelector(selectCountriesPageDomain, substate => substate.toJS());

export default makeSelectCountriesPage;
export { selectCountriesPageDomain };
