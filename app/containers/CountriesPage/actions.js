/*
 *
 * CountriesPage actions
 *
 */

import {
  ADD_TO_FILTERED_LIST,
  DEFAULT_ACTION,
  GET_ALL_COUNTRIES,
  GET_ALL_COUNTRIES_SUCCESS,
  GET_COUNTRY_BY_ARRAY_MATCHED,
  GET_COUNTRY_BY_ARRAY_MATCHED_SUCCESS,
  GET_COUNTRY_BY_FULL_NAME,
  GET_COUNTRY_BY_FULL_NAME_SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getAllCountries() {
  return {
    type: GET_ALL_COUNTRIES,
  };
}
export function getAllCountriesSuccess(payload) {
  return {
    type: GET_ALL_COUNTRIES_SUCCESS,
    payload,
  };
}

export function addToFilteredList(payload) {
  return {
    type: ADD_TO_FILTERED_LIST,
    payload,
  };
}

export function getCountryByFullName(payload) {
  return {
    type: GET_COUNTRY_BY_FULL_NAME,
    payload,
  };
}

export function getCountryByFullNameSuccess(payload) {
  return {
    type: GET_COUNTRY_BY_FULL_NAME_SUCCESS,
    payload,
  };
}

export function getCountriesByArrayMatched() {
  return {
    type: GET_COUNTRY_BY_ARRAY_MATCHED,
  };
}
export function getCountriesByArrayMatchedSuccess(payload) {
  return {
    type: GET_COUNTRY_BY_ARRAY_MATCHED_SUCCESS,
    payload,
  };
}
