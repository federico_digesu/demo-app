/**
 *
 * Asynchronously loads the component for CountriesPage
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
