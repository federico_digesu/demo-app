import { fromJS } from 'immutable';
import countriesPageReducer from '../reducer';

describe('countriesPageReducer', () => {
  it('returns the initial state', () => {
    expect(countriesPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
