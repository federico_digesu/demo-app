/**
 *
 * CountriesPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectCountriesPage from './selectors';
import {
  getAllCountries,
  addToFilteredList,
  getCountryByFullName,
  getCountriesByArrayMatched,
} from './actions';
import reducer from './reducer';
import saga from './saga';

/* eslint-disable react/prefer-stateless-function */
export class CountriesPage extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(getAllCountries());
    dispatch(getCountriesByArrayMatched());
  }

  onChange = evt => {
    const { dispatch } = this.props;
    dispatch(addToFilteredList(evt.target.value));
  };

  getCountryDetails = country => {
    const { dispatch } = this.props;
    dispatch(getCountryByFullName(country));
  };

  render() {
    const {
      countries,
      filtered,
      country,
      strings,
      countriesStringArrayMatched,
    } = this.props.countriesPage;
    return (
      <div>
        <Helmet>
          <title>CountriesPage</title>
          <meta name="description" content="Description of CountriesPage" />
        </Helmet>
        <div>
          <h3>Showing All Countries</h3>
          <div>
            <input
              placeholder="Filter Country By Name"
              type="text"
              onChange={this.onChange}
            />
          </div>
        </div>
        <div className="countries_container">
          <div className="country_lists">
            <h3>All Countries (Question 3)</h3>
            {filtered.length > 0 ? (
              <div>
                {filtered.map(c => (
                  <div key={c}>
                    <button
                      onClick={() => this.getCountryDetails(c)}
                      type="button"
                      className="country_name"
                    >
                      {c}
                    </button>
                  </div>
                ))}
              </div>
            ) : (
              <div>
                {countries.map(c => (
                  <div key={c}>
                    <button
                      onClick={() => this.getCountryDetails(c)}
                      type="button"
                      className="country_name"
                    >
                      {c}
                    </button>
                  </div>
                ))}
              </div>
            )}
          </div>
          <div>
            <h3>Click on a Country to See Details. (Question 1)</h3>
            <div>
              <p>name: {country.name}</p>
              <p>alpha2Code: {country.alpha2Code}</p>
              <p>capital: {country.capital}</p>
              <p>region: {country.region}</p>
              <p>population: {country.population}</p>
            </div>
          </div>
          <div>
            <h3>Countries By Strings Array Matched (Question 2)</h3>
            <div>String Array: {JSON.stringify(strings)}</div>
            <div>
              {countriesStringArrayMatched.map(c => (
                <div key={c}>
                  <button
                    onClick={() => this.getCountryDetails(c)}
                    type="button"
                    className="country_name"
                  >
                    {c}
                  </button>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CountriesPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  countriesPage: PropTypes.shape({
    countries: PropTypes.array,
    filtered: PropTypes.array,
    country: PropTypes.object,
    strings: PropTypes.array,
    countriesStringArrayMatched: PropTypes.array,
  }).isRequired,
};

const mapStateToProps = createStructuredSelector({
  countriesPage: makeSelectCountriesPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'countriesPage', reducer });
const withSaga = injectSaga({ key: 'countriesPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(CountriesPage);
