/*
 *
 * CountriesPage constants
 *
 */

export const DEFAULT_ACTION = 'app/CountriesPage/DEFAULT_ACTION';
export const GET_ALL_COUNTRIES = 'app/CountriesPage/GET_ALL_COUNTRIES';
export const GET_ALL_COUNTRIES_SUCCESS =
  'app/CountriesPage/GET_ALL_COUNTRIES_SUCCESS';
export const ADD_TO_FILTERED_LIST = 'app/CountriesPage/ADD_TO_FILTERED_LIST';
export const GET_COUNTRY_BY_FULL_NAME =
  'app/CountriesPage/GET_COUNTRY_BY_FULL_NAME';
export const GET_COUNTRY_BY_FULL_NAME_SUCCESS =
  'app/CountriesPage/GET_COUNTRY_BY_FULL_NAME_SUCCESS';
export const GET_COUNTRY_BY_ARRAY_MATCHED =
  'app/CountriesPage/GET_COUNTRY_BY_ARRAY_MATCHED';
export const GET_COUNTRY_BY_ARRAY_MATCHED_SUCCESS =
  'app/CountriesPage/GET_COUNTRY_BY_ARRAY_MATCHED_SUCCESS';
