import { takeLatest, put, all } from 'redux-saga/effects';
import axios from 'utils/axios';
import { toast } from 'react-toastify';
import {
  GET_ALL_COUNTRIES,
  GET_COUNTRY_BY_ARRAY_MATCHED,
  GET_COUNTRY_BY_FULL_NAME,
} from './constants';
import {
  getAllCountriesSuccess,
  getCountryByFullNameSuccess,
  getCountriesByArrayMatchedSuccess,
} from './actions';

function* getCountries() {
  try {
    const res = yield axios.get('countries');
    yield put(getAllCountriesSuccess(res.data));
  } catch (error) {
    yield toast.error(error.response.data);
  }
}
function* getCountriesByArrayMatched() {
  try {
    const res = yield axios.get('countries/getByArrayMatched');
    yield put(getCountriesByArrayMatchedSuccess(res.data));
  } catch (error) {
    yield toast.error(error.response.data);
  }
}

function* getCountryByFullName(action) {
  try {
    const res = yield axios.get(
      `countries/getCountryByFullName/${action.payload}`,
    );
    yield put(getCountryByFullNameSuccess(res.data));
  } catch (error) {
    yield toast.error(error.response.data);
  }
}

function* watchGetCountries() {
  yield takeLatest(GET_ALL_COUNTRIES, getCountries);
}
function* watchGetCountriesByArrayMatched() {
  yield takeLatest(GET_COUNTRY_BY_ARRAY_MATCHED, getCountriesByArrayMatched);
}

function* watchGetCountryByFullName() {
  yield takeLatest(GET_COUNTRY_BY_FULL_NAME, getCountryByFullName);
}
// Individual exports for testing
export default function* countriesPageSaga() {
  // See example in containers/HomePage/saga.js
  yield all([
    watchGetCountries(),
    watchGetCountryByFullName(),
    watchGetCountriesByArrayMatched(),
  ]);
}
