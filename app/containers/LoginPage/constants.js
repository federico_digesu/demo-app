/*
 *
 * LoginPage constants
 *
 */

export const DEFAULT_ACTION = 'app/LoginPage/DEFAULT_ACTION';
export const LOGIN_SUBMIT = 'app/LoginPage/LOGIN_SUBMIT';
