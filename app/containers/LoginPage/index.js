/**
 *
 * LoginPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import LoginForm from 'components/LoginForm';
import { handleSubmit } from './actions';
import makeSelectLoginPage from './selectors';
import reducer from './reducer';
import saga from './saga';

/* eslint-disable react/prefer-stateless-function */
export class LoginPage extends React.Component {
  submit = values => {
    const { dispatch } = this.props;
    dispatch(handleSubmit(values));
  };

  render() {
    return (
      <div className="login_page">
        <Helmet>
          <title>Slot Machine: LoginPage</title>
          <meta name="description" content="Description of LoginPage" />
        </Helmet>
        <div>
          <h2>Welcome to Game</h2>
        </div>
        <LoginForm submitFunc={this.submit} />
      </div>
    );
  }
}

LoginPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loginPage: makeSelectLoginPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'loginPage', reducer });
const withSaga = injectSaga({ key: 'loginPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LoginPage);
