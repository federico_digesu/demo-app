import { takeLatest, put, all } from 'redux-saga/effects';
import { stopSubmit } from 'redux-form';
import { push } from 'connected-react-router';
import axios from 'utils/axios';
import { setLocalStorageToken } from 'utils/localStorage';
import { setUser } from 'containers/App/actions';
import { LOGIN_SUBMIT } from './constants';

function* handleSubmit(action) {
  const payload = action.payload.toJS();
  try {
    const res = yield axios.post('/auth/login', payload);
    if (res.status === 201) {
      setLocalStorageToken(res.data.token);
      yield put(setUser(res.data.user));
      yield put(push('/'));
    }
  } catch (error) {
    if (error.response.status === 409) {
      yield put(stopSubmit('loginForm', error.response.data));
    }
  }
}

function* watchSubmit() {
  yield takeLatest(LOGIN_SUBMIT, handleSubmit);
}
// Individual exports for testing
export default function* loginPageSaga() {
  yield all([watchSubmit()]);
}
