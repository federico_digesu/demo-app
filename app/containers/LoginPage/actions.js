/*
 *
 * LoginPage actions
 *
 */

import { DEFAULT_ACTION, LOGIN_SUBMIT } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function handleSubmit(payload) {
  return {
    type: LOGIN_SUBMIT,
    payload,
  };
}
