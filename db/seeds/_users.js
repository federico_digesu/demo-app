exports.seed = knex => {
  // Deletes ALL existing entries
  const tableName = 'users';
  return knex(tableName)
    .del()
    .then(() =>
      // Inserts seed entries
      knex(tableName).insert([
        {

        },
      ]),
    );
};
