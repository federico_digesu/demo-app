exports.up = function (knex, Promise) { // eslint-disable-line
  const createUsers = table => {
    table.increments();
    table.string('name', 50).notNullable();
    table.string('email', 40);
    table.string('password', 150);
  };

  /*
    eslint-disable
   */
  return knex.schema
    .createTable('users', createUsers)
};

exports.down = function (knex, Promise) {
};
/* eslint-enable */
