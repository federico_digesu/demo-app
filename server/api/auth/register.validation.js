const validate = require('validate.js');
const Promise = require('bluebird');
const knex = require('./../_config/knex');

function basic(inputs) {
  const conditions = {
    name: {
      presence: {
        message: '^Name is Required.',
      },
      length: {
        minimum: 4,
        tooShort: '^minimum 4 letter.',
        maximum: 50,
        tooLong: '^maximum 50 letter allowed.',
        tokenizer(value) {
          return value.trim();
        },
      },
    },
    email: {
      presence: {
        message: '^Email is Required.',
      },
      email: {
        message: "doesn't look like a valid email",
      },
    },
    password: {
      presence: {
        message: '^Password is Required.',
      },
      length: {
        minimum: 6,
        tooShort: '^Minimum 6 letter.',
        maximum: 15,
        tooLong: '^Maximum 15 letter allowed.',
        tokenizer(value) {
          return value.trim();
        },
      },
    },
    repeat_password: {
      presence: {
        message: '^Repeat Password is Required.',
      },
      equality: 'password',
    },
  };

  return validate.async(inputs, conditions);
}

function emailExistedCheck(inputs) {
  return new Promise((resolve, reject) => {
    knex('users')
      .where('email', inputs.email)
      .first()
      .then(found => {
        if (found) {
          reject({ email: ['This email is already registered.'] }); // eslint-disable-line
        } else {
          resolve('Valid Email');
        }
      });
  });
}

module.exports = {
  basic,
  emailExistedCheck,
};
