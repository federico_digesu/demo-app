const passport = require('passport');
const passportJWT = require('passport-jwt');
const cfg = require('./../_config/_config');
const knex = require('./../_config/knex');
const ExtractJwt = passportJWT.ExtractJwt;
const Strategy = passportJWT.Strategy;
const params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJwt.fromAuthHeader()
};

module.exports = () => {
  const strategy = new Strategy(params, (payload, done) => { //eslint-disable-line
    const payloadUserId = payload.id;
    if (payloadUserId) {
      knex('contacts')
        .where('id', payloadUserId)
        .first()
        .then((contact) => {
          if (contact) {
            return done(null, {
              id: contact.id
            });
          }
          return done(new Error('User not found'), null);
        });
    } else {
      // todo: fixError: return 401 instead of throwing error.
      return done(new Error('No Id on Token.'), null);
    }
  });
  passport.use(strategy);
  return {
    initialize() {
      return passport.initialize();
    },
    authenticate() {
      return passport.authenticate('jwt', cfg.jwtSession);
    }
  };
};
