const express = require('express');
const Auth = express.Router(); // eslint-disable-line

Auth.post('/login', require('./login'));
Auth.get('/getUserInfo/:token', require('./getUserInfo'));
Auth.post('/register', require('./register'));
// Auth.get('/checkAuthToken/:token', require('./checkAuthToken'));

module.exports = Auth;
