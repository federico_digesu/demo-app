const jwt = require('jwt-simple');
const bcrypt = require('bcryptjs');
const knex = require('./../_config/knex');
const loginValidation = require('./login.validation');

const cfg = require('./../_config/_config');

function login(req, res) {
  const { body } = req;
  loginValidation
    .basic(body)
    .then(() => {
      // valid
      const { email, password } = body;
      return knex('users')
        .where('email', email)
        .first()
        .then(user => {
          if (user) {
            return bcrypt.compare(password, user.password).then(matched => {
              if (matched) {
                // return jwt
                const token = jwt.encode(
                  {
                    id: user.id,
                  },
                  cfg.jwtSecret,
                );
                return res.status(201).json({
                  token,
                  user: {
                    id: user.id,
                    email: user.email,
                    name: user.name,
                  },
                });
              }
              return res
                .status(409)
                .json({ _error: 'Email and Password not matched.' });
            });
          }
          return res
            .status(409)
            .json({ _error: 'Email and Password not matched.' });
        });
    })
    .catch(error => {
      res.status(409).json(error);
    });
}

module.exports = login;
