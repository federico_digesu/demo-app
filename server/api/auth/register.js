// const jwt = require('jwt-simple');
const bcrypt = require('bcryptjs');
const knex = require('./../_config/knex');
const registerValidation = require('./register.validation');
const config = require('./../_config/_config');

function register(req, res) {
  const { body } = req;
  registerValidation
    .basic(body)
    .then(() => registerValidation.emailExistedCheck(body))
    .then(() => {
      // form inputs is valid
      const { name, email, password } = body;
      return bcrypt.hash(password, 8).then(hash =>
        knex('users')
          .insert({
            name,
            email,
            password: hash,
          })
          .then(() => {
            res.status(201).json('User Created.');
          }),
      );
    })
    .catch(err => {
      res.status(409).json(err);
    });
}

module.exports = register;
