const validate = require('validate.js');

function basic(inputs) {
  const conditions = {
    email: {
      presence: {
        message: '^Email is Required.',
      },
    },
    password: {
      presence: {
        message: '^Password is Required.',
      },
    },
  };

  return validate.async(inputs, conditions);
}

module.exports = {
  basic,
};
