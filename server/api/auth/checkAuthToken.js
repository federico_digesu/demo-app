const jwt = require('jwt-simple');
const knex = require('./../_config/knex');
const cfg = require('./../_config/_config');

function checkAuthToken(req, res) {
  const { token } = req.params;
  if (token) {
    try {
      const decoded = jwt.decode(token, cfg.jwtSecret);
      if (decoded.id) {
        knex('contacts')
          .where('id', decoded.id)
          .first()
          .then(contact => {
            if (contact) {
              res.json({
                user: {
                  name: contact.name,
                },
              });
            } else {
              res.status(422).json();
            }
          });
      } else {
        res.status(422).json();
      }
    } catch (err) {
      res.status(422).json();
    }
  } else {
    res.status(422).json();
  }
}

module.exports = checkAuthToken;
