const jwt = require('jwt-simple');
const knex = require('./../_config/knex');

const cfg = require('./../_config/_config');

function getUserInf(req, res) {
  const { token } = req.params;
  if (token) {
    try {
      const decoded = jwt.decode(token, cfg.jwtSecret);
      knex('users')
        .where('id', decoded.id)
        .first()
        .then(user => {
          if (user) {
            res.json({
              id: user.id,
              name: user.name,
              email: user.email,
            });
          } else {
            res.status(409).json({
              type: 'invalidUser',
              message: 'User is removed or deactivated.',
            });
          }
        });
    } catch (error) {
      res.status(409).json({
        type: 'invalidToken',
        message: 'Token is not valid.',
      });
    }
  } else {
    res.status(409).json({
      type: 'noToken',
      message: 'No User Token Found.',
    });
  }
}

module.exports = getUserInf;
