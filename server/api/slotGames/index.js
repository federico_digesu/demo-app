const express = require('express');
const Router = express.Router(); // eslint-disable-line

Router.get('/', require('./getWheels'));
Router.post('/calculatePoints', require('./calculatePoints'));

module.exports = Router;
