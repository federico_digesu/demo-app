const config = require('./../_config/_config');
module.exports = async (req, res) => {
  const { body: inputs } = req;
  const { wheel1, wheel2, wheel3 } = config.wheels;
  const resultString = [
    wheel1[inputs[0]],
    wheel2[inputs[1]],
    wheel3[inputs[2]],
  ];

  const refCounter = {};
  for (const result of resultString) { // eslint-disable-line
    if (refCounter[result]) {
      refCounter[result] += 1;
    } else {
      refCounter[result] = 1;
    }
  }

  let point = 0;
  for (let key of Object.keys(refCounter)) {// eslint-disable-line
    const matchString = key + refCounter[key];
    if (
      config.pointsTable[matchString] &&
      config.pointsTable[matchString] > point
    ) {
      point = config.pointsTable[matchString];
    }
  }
  res.json({
    point,
    resultString,
  });
};
