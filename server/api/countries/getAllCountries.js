const countryService = require('./../_services/restcountries');
module.exports = async (req, res) => {
  try {
    let countries = await countryService.getAllCountries();
    countries = countries.map(country => country.name);
    res.json(countries);
  } catch (err) {
    res.status(422).json('Unable To Get Countries');
  }
};
