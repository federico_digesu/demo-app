const countryService = require('./../_services/restcountries');
module.exports = async (req, res) => {
  try {
    const { countryName } = req.params;
    const country = await countryService.getCountryByFullName(countryName);
    res.json(country);
  } catch (err) {
    res.status(422).json('Unable To Get Country');
  }
};
