const express = require('express');
const Router = express.Router(); // eslint-disable-line

Router.get('/', require('./getAllCountries'));
Router.get('/getByArrayMatched', require('./getByArrayMatched'));
Router.get(
  '/getCountryByFullName/:countryName',
  require('./getCountryByFullName'),
);

module.exports = Router;
