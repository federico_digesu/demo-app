const countryService = require('./../_services/restcountries');
module.exports = async (req, res) => {
  try {
    let countries = await countryService.getAllCountries();
    countries = countries.map(country => country.name);
    const strings = ['al', 'ust', 'go'];
    countries = countries.filter(country => {
      for (const str of strings) { // eslint-disable-line
        if (country.toLowerCase().indexOf(str) > -1) {
          return true;
        }
      }
      return false;
    });
    res.json({
      strings,
      countries,
    });
  } catch (err) {
    res.status(422).json('Unable To Get Countries');
  }
};
