const knex = require('knex');
const config = require('./_config');

const dbConfig = config.DB.development;

module.exports = knex(dbConfig);
