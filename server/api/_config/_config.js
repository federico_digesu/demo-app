const migrationDirectory = `${__dirname}../../../../db/migrations`;
const seedDirectory = `${__dirname}../../../../db/seeds`;

module.exports = {
  DB: {
    development: {
      client: 'sqlite3',
      connection: {
        filename: './db/db.sqlite',
      },
      migrations: {
        directory: migrationDirectory,
      },
      seeds: {
        directory: seedDirectory,
      },
      debug: false,
      pool: {
        min: 0,
        max: 7,
      },
    },
    pool: {
      min: 0,
      max: 7,
    },
  },
  host: '127.0.0.1',
  port: 3030,
  jwtSecret: 'xy5h83@8aadf43klkjadbnfladfiuoerlj@@kshe',
  jwtSession: {
    session: false,
  },
  wheels: {
    wheel1: [
      'cherry',
      'lemon',
      'apple',
      'lemon',
      'banana',
      'banana',
      'lemon',
      'lemon',
    ],
    wheel2: [
      'lemon',
      'apple',
      'lemon',
      'lemon',
      'cherry',
      'apple',
      'banana',
      'lemon',
    ],
    wheel3: [
      'lemon',
      'apple',
      'lemon',
      'apple',
      'cherry',
      'lemon',
      'banana',
      'lemon',
    ],
  },
  pointsTable: {
    cherry3: 50,
    cherry2: 40,
    apple3: 20,
    apple2: 10,
    banana3: 15,
    banana2: 5,
    lemon3: 3,
  },
};
