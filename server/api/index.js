const express = require('express');
const jwt = require('jwt-simple');
const knex = require('./_config/knex');
const Api = express.Router(); // eslint-disable-line
const packageJson = require('./../../package.json');
const cfg = require('./_config/_config');

const auth = require('./auth');
Api.use((req, res, next) => {
  if (!req.get('Origin')) return next();
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,uid');
  if (req.method === 'OPTIONS') return res.status(200).json('ok');
  return next();
});
Api.get('/', (req, res) => {
  res.json({
    status: 'API is working',
    authors: [
      {
        name: 'Federico Di Gesu',
        email: 'Federico Di Gesu <federico.digesu@gmail.com>',
        description:
          'Initialize App and Primary Level Developer.(02-2019 to present)',
      },
    ],
    version: packageJson.version,
    env: process.env.NODE_ENV,
  });
});

async function authMiddleware(req, res, next) {
  const authorizationHeader = req.header('Authorization');
  if (authorizationHeader) {
    try {
      const decoded = jwt.decode(
        authorizationHeader.split(' ')[1],
        cfg.jwtSecret,
      );
      if (decoded.id) {
        const user = await knex('users')
          .columns(['id'])
          .where('id', decoded.id)
          .first();
        if (user) {
          req.user = user;
          next();
        } else {
          res.status(403).json('User Is Not Active or Exists.');
        }
      } else {
        res.status(403).json('Something is not right about your token claim.');
      }
    } catch (e) {
      res.status(403).json('Something is not right about your token claim.');
    }
  } else {
    res.status(403).json('You Are Not Authorized To Access Here.');
  }
}

Api.use('/auth', auth);
Api.use('/countries', authMiddleware, require('./countries'));
Api.use('/slotGames', authMiddleware, require('./slotGames'));
module.exports = Api;
