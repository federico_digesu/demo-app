// const validatejs = require('validate.js');
const knex = require('../_config/knex');
//
module.exports = async (tableName, fieldName, fieldValue, id) =>
  new Promise((resolve, reject) => {
    let query = knex(tableName).where(fieldName, fieldValue);
    if (id) {
      query = query.whereNot('id', id);
    }
    return query.first().then(found => {
      if (found) {
        reject({ [fieldName]: [`${fieldValue} is already exists on database.`] }); // eslint-disable-line
      } else {
        resolve(undefined);
      }
    });
  });
