const axios = require('axios');
let countries;
module.exports.getAllCountries = async () => {
  try {
    if (countries) {
      return countries;
    }
    const req = await axios.get('https://restcountries.eu/rest/v2/all');
    countries = req.data;
    return countries;
  } catch (err) {
    throw Error('Unable To connect to API.');
  }
};
module.exports.getCountryByFullName = async name => {
  try {
    const req = await axios.get(
      `https://restcountries.eu/rest/v2/name/${name}?fullText=true`,
    );
    return req.data[0];
  } catch (err) {
    throw Error('Unable To connect to API.');
  }
};
